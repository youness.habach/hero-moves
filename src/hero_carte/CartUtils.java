package hero_carte;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

final class CartUtils {

    private static final int COLS = 20;
    private static final int ROWS = 20;

    private static List<String> transformFileToLines(String filename) {
        List<String> linesList = new ArrayList<>();
        try {
            File cartFile = new File(filename);
            Scanner cartReader = new Scanner(cartFile);
            while (cartReader.hasNextLine()) {
                linesList.add(cartReader.nextLine());
            }
            cartReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return linesList;
    }

    static char[][] getMatrixFromLines(String filename) {
        char[][] cartMatrix = new char[COLS][ROWS];
        List<String> lines = transformFileToLines(filename);

        int index = 0;
        for (String line: lines) {
            cartMatrix[index] = line.toCharArray();
            index++;
        };

        return cartMatrix;
    }

    static boolean checkIfNextStepIsWood(char[][] matrixFromLines, int x, int y) {
        return matrixFromLines[x][y] == '#';
    }

    static boolean checkIfInitialStepIsWood(char[][] matrixFromLines, Position initialPosition) {
        return matrixFromLines[initialPosition.getX()][initialPosition.getY()] == '#';
    }
}
