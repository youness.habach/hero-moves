package hero_carte;

import java.util.Arrays;

import static hero_carte.CartUtils.checkIfNextStepIsWood;


public class Hero {
    private Position initialPosition;
    private Position finalPosition;
    private char[] steps;

    public Hero() {
    }

    public Hero(Position initialPosition, char[] steps) {
        this.initialPosition = initialPosition;
        this.steps = steps;
    }

    public Position getInitialPosition() {
        return initialPosition;
    }

    public void setInitialPosition(Position initialPosition) {
        this.initialPosition = initialPosition;
    }

    public char[] getSteps() {
        return steps;
    }

    public void setSteps(char[] steps) {
        this.steps = steps;
    }

    public Position getFinalPosition() {
        return finalPosition;
    }

    public void setFinalPosition(Position finalPosition) {
        this.finalPosition = finalPosition;
    }

    public void setStepsFromString(String steps) {
        // TODO need to add some limitation if user enter not allowed chars
        char[] allowedSteps = {'N', 'S', 'E', 'O'};
        this.steps = steps.toCharArray();
    }

    public void move(char[][] matrixFromLines) {
        if (CartUtils.checkIfInitialStepIsWood(matrixFromLines, this.initialPosition)) {
            // TODO try catch
            throw new IllegalArgumentException("PROBLEM OCCURRED : Initial position is a wood");
        }
        Position heroPosition = new Position(this.initialPosition);
        char[] steps = this.steps;

        for (char step : steps) {
            switch (step) {
                case StepPosition.WEST:
                    if (!checkIfNextStepIsWood(matrixFromLines, heroPosition.getX(), heroPosition.getY() - 1)) {
                        heroPosition.decrementY();
                    }
                    break;
                case StepPosition.EST:
                    if (!checkIfNextStepIsWood(matrixFromLines, heroPosition.getX(), heroPosition.getY() + 1)) {
                        heroPosition.incrementY();
                    }
                    break;
                case StepPosition.NORTH:
                    if (!checkIfNextStepIsWood(matrixFromLines, heroPosition.getX() - 1, heroPosition.getY())) {
                        heroPosition.decrementX();
                    }
                    break;
                case StepPosition.SUD:
                    if (!checkIfNextStepIsWood(matrixFromLines, heroPosition.getX() + 1, heroPosition.getY())) {
                        heroPosition.incrementX();
                    }
                    break;
            }
        }
        this.finalPosition = heroPosition;
    }

    @Override
    public String toString() {
        return "Hero{" +
                "initialPosition=" + initialPosition +
                ", finalPosition=" + finalPosition +
                ", steps=" + Arrays.toString(steps) +
                '}';
    }
}
