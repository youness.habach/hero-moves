package hero_carte;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        char[][] matrixFromLines = CartUtils.getMatrixFromLines("src/hero_carte/cart.txt");

        String gameCommand;
        Scanner scanner = new Scanner(System.in);
        Hero playingHero = new Hero();
        System.out.println("--------------------------------------------------------------");
        System.out.println("|                                                            |");
        System.out.println("|     THIS IS A GAME TO MOVE OUR HERO TO A SPECIFIC          |");
        System.out.println("|           LOCATION IN THE CART USING KEYWORD               |");
        System.out.println("|                                                            |");
        System.out.println("--------------------------------------------------------------");
        System.out.println("|     PRESS : [start] to play and [exit] to exit the game    |");
        System.out.println("--------------------------------------------------------------");

        while(true) {
            System.out.print("Press [start] to play or [exit] to exit : ");
            gameCommand = scanner.nextLine();
            if (gameCommand.equals("start")) {
                System.out.println("Fill the coordinates");
                System.out.print("X = ");
                Integer x = scanner.nextInt();
                System.out.print("Y = ");
                Integer y = scanner.nextInt();

                playingHero.setInitialPosition(new Position(y, x));
                System.out.print("Fill the steps : ");
                scanner.nextLine();
                String steps = scanner.nextLine();
                playingHero.setStepsFromString(steps);

                playingHero.move(matrixFromLines);
                System.out.println("You are in the position : " + playingHero.getFinalPosition().toString() + "\n");
            } else if (gameCommand.equals("exit")) {
                System.out.println("Exiting -- Good by");
                return;
            } else {
                System.out.println("Try again");
            }
        }
    }
}
